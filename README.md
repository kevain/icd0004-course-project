# ICD0004 Course Project

Author: Kenert Vaino \
179221 IADB \
email: kevain@taltech.ee 

# WeatherApp
A simple Java application, which takes in the name of a city and then gets it's weather data using Open Weather Map API.

## Requirements
Program was developed using Java 11 SDK, therefore it is recommended to be ran using said version as well. The author does not quarantee compatibility with lower Java versions.

## How to run
1. Clone this repository
2. Navigate to project directry. There you will find a file titled "cities.txt". Enter the city name for which you wish to receive weather information. You can enter multiple cities (one per line) - a weather report will be generated for all of them.
3. Run the program from Main class.
4. Once the execution has finished, you will find the weather data for cities you provided in the project directory.

## Running Tests
To run all tests for the weatherApp, navigate to project directory in terminal and issue the following command:
```
> gradlew clean weatherAppTests --info
```

To run all TalTech web-page tests, use the following command:
```
> gradlew clean taltechWebTests --info  
```

## Examples
### Input
File: cities.txt
```text
Tallinn
```

### Output
File: WeatherReport-Tallinn.json
```json
{
  "weatherReportDetails" : {
    "city" : "Tallinn",
    "coordinates" : "59.440000,24.750000",
    "temperatureUnit" : "Celsius"
  },
  "currentWeatherReport" : {
    "temperature" : -2.75,
    "humidity" : 73.0,
    "pressure" : 1026.0
  },
  "forecastReport" : [ {
    "date" : "2019-11-25",
    "weather" : {
      "temperature" : -3.07,
      "humidity" : 67.0,
      "pressure" : 1024.0
    }
  }, {
    "date" : "2019-11-26",
    "weather" : {
      "temperature" : -1.38,
      "humidity" : 66.0,
      "pressure" : 1019.0
    }
  }, {
    "date" : "2019-11-27",
    "weather" : {
      "temperature" : 0.66,
      "humidity" : 90.0,
      "pressure" : 1009.0
    }
  } ]
}
```

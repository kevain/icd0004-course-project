package ee.itcollege.kevain.web.page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class StudyInformationPage {

    public static final String URL = "https://taltech.ee/tudengile/oppeinfo/";

    private By studyInformationHeader = By.cssSelector("h2");

    private WebDriver webDriver;

    public StudyInformationPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public String getHeaderText() {
        return webDriver.findElement(studyInformationHeader).getText();
    }
}

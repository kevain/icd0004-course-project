package ee.itcollege.kevain.web.page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LecturerSearchPage {

    public static String URL = "https://taltech.ee/tootaja-otsing/";

    private By lecturerNameField = By.xpath("/html/body/div[4]/div/form/table/tbody/tr[1]/td[2]/input");
    private By searchButton = By.xpath("/html/body/div[4]/div/form/table/tbody/tr[4]/td[2]/input");
    private By emailValueField = By.xpath("/html/body/div[4]/div/table/tbody/tr[3]/td[2]/a");

    private WebDriver webDriver;

    public LecturerSearchPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void enterLecturerName(String name) {
        webDriver.findElement(lecturerNameField).sendKeys(name);
    }

    public void clickSearchButton() {
        webDriver.findElement(searchButton).click();
    }

    public String getLecturerEmailValue() {
        return webDriver.findElement(emailValueField).getText();
    }

}
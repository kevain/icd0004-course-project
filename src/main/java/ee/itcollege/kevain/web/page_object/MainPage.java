package ee.itcollege.kevain.web.page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class MainPage {

    public static String URL = "https://taltech.ee/";

    private By loginDropDown = By.cssSelector("#dropdownHeader > div > div.login.languages > ul > li > a");

    private By goToOisButton = By.cssSelector("#dropdownHeader > div > div.login.languages > ul > li > ul > li:nth-child(1) > a");
    private By goToMoodleButton = By.cssSelector("#dropdownHeader > div > div.login.languages > ul > li > ul > li:nth-child(2) > a");
    private By goToSiseveebButton = By.cssSelector("#dropdownHeader > div > div.login.languages > ul > li > ul > li:nth-child(3) > a");

    private WebDriver webDriver;

    public MainPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    private void hoverOverLoginDropDown(){
        new Actions(webDriver).moveToElement(webDriver.findElement(loginDropDown)).build().perform();
    }

    public void clickGoToOisButton() {
        hoverOverLoginDropDown();
        webDriver.findElement(goToOisButton).click();
    }

    public void clickGoToMoodleButton() {
        hoverOverLoginDropDown();
        webDriver.findElement(goToMoodleButton).click();
    }

    public void clickGoToSiseveebButton() {
        hoverOverLoginDropDown();
        webDriver.findElement(goToSiseveebButton).click();
    }
}

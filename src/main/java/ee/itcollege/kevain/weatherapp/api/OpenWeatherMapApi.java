package ee.itcollege.kevain.weatherapp.api;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import ee.itcollege.kevain.weatherapp.api.dto.CurrentWeatherDto;
import ee.itcollege.kevain.weatherapp.api.dto.WeatherForecastDto;
import ee.itcollege.kevain.weatherapp.exception.InvalidCityGivenException;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;

import static com.sun.jersey.api.client.Client.create;
import static com.sun.jersey.api.json.JSONConfiguration.FEATURE_POJO_MAPPING;

public class OpenWeatherMapApi {
    private static final String API_KEY = "eb40017d5dd2e850ceeab9c47a80b78b";

    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5";
    private static final String ENDPOINT_WEATHER = "/weather";
    private static final String ENDPOINT_FORECAST = "/forecast";

    private static Client client = getConfiguredClient();

    private static Client getConfiguredClient() {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getClasses().add(JacksonJaxbJsonProvider.class);
        clientConfig.getFeatures().put(FEATURE_POJO_MAPPING, Boolean.TRUE);
        return create(clientConfig);
    }

    public CurrentWeatherDto getCurrentWeatherDataForCity(String city) throws InvalidCityGivenException {
        if (isCityNameInvalid(city)) {
            throw new InvalidCityGivenException("Invalid city name! Cannot be null or empty");
        }

        ClientResponse clientResponse = getCurrentWeather(city);

        if (clientResponse.getStatus() == 404) {
            throw new InvalidCityGivenException("Could not find such city!");
        } else return clientResponse.getEntity(CurrentWeatherDto.class);
    }

    public WeatherForecastDto getWeatherForecastForCity(String city) throws InvalidCityGivenException {
        if (isCityNameInvalid(city)) {
            throw new InvalidCityGivenException("Invalid city name! Cannot be null or empty!");
        }

        ClientResponse clientResponse = getWeatherForecast(city);

        if (clientResponse.getStatus() == 404) {
            throw new InvalidCityGivenException("Could not find such city!");
        } else return clientResponse.getEntity(WeatherForecastDto.class);
    }

    public ClientResponse getCurrentWeather(String cityName) {
        return client.resource(BASE_URL + ENDPOINT_WEATHER)
                .queryParam("q", cityName)
                .queryParam("APPID", API_KEY)
                .queryParam("units", "metric")
                .get(ClientResponse.class);
    }

    public ClientResponse getWeatherForecast(String cityName) {
        return client.resource(BASE_URL + ENDPOINT_FORECAST)
                .queryParam("q", cityName)
                .queryParam("APPID", API_KEY)
                .queryParam("units", "metric")
                .get(ClientResponse.class);
    }

    private boolean isCityNameInvalid(String city) {
        return city == null || city.isEmpty();
    }

}

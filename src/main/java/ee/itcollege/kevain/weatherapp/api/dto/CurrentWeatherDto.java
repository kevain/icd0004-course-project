package ee.itcollege.kevain.weatherapp.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentWeatherDto {
    private String name;
    private CoordinatesDto coord;
    private WeatherDto main;

}

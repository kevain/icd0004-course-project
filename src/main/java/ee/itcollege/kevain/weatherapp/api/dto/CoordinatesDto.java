package ee.itcollege.kevain.weatherapp.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CoordinatesDto {
    private double lat;
    private double lon;
}

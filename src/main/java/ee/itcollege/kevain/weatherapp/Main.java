package ee.itcollege.kevain.weatherapp;

import ee.itcollege.kevain.weatherapp.model.WeatherReport;
import ee.itcollege.kevain.weatherapp.util.FileManager;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        List<String> cities = FileManager.readCityNamesFromFile("cities.txt");

        WeatherApp weatherApp = new WeatherApp();
        for (String city: cities) {
            try {
                WeatherReport weatherReport = weatherApp.getWeatherReportForCity(city);
                FileManager.writeWeatherReportToFile(weatherReport);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

package ee.itcollege.kevain.weatherapp;

import ee.itcollege.kevain.weatherapp.api.OpenWeatherMapApi;
import ee.itcollege.kevain.weatherapp.api.dto.CurrentWeatherDto;
import ee.itcollege.kevain.weatherapp.api.dto.ForecastDto;
import ee.itcollege.kevain.weatherapp.api.dto.WeatherForecastDto;
import ee.itcollege.kevain.weatherapp.exception.CurrentWeatherDataMissingException;
import ee.itcollege.kevain.weatherapp.exception.InvalidCityGivenException;
import ee.itcollege.kevain.weatherapp.exception.WeatherForecastDataMissingException;
import ee.itcollege.kevain.weatherapp.model.Forecast;
import ee.itcollege.kevain.weatherapp.model.Weather;
import ee.itcollege.kevain.weatherapp.model.WeatherReport;
import ee.itcollege.kevain.weatherapp.model.WeatherReportDetails;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WeatherApp {

    private OpenWeatherMapApi openWeatherMapApi;

    public WeatherApp() {
        this.openWeatherMapApi = new OpenWeatherMapApi();
    }

    public WeatherApp(OpenWeatherMapApi openWeatherMapApi) {
        this.openWeatherMapApi = openWeatherMapApi;
    }

    public WeatherReport getWeatherReportForCity(String city) throws InvalidCityGivenException,
            CurrentWeatherDataMissingException, WeatherForecastDataMissingException {
        if (city == null || city.isEmpty()) {
            throw new InvalidCityGivenException("Cannot get weather details without a provided city.");
        }

        WeatherReport weatherReport = new WeatherReport();

        CurrentWeatherDto currentWeatherDto = openWeatherMapApi.getCurrentWeatherDataForCity(city);
        if (currentWeatherDto == null) {
            throw new CurrentWeatherDataMissingException();
        }

        WeatherForecastDto weatherForecastDto = openWeatherMapApi.getWeatherForecastForCity(city);
        if (weatherForecastDto == null) {
            throw new WeatherForecastDataMissingException();
        }

        weatherReport.setCurrentWeatherReport(getCurrentWeather(currentWeatherDto));
        weatherReport.setWeatherReportDetails(getWeatherReportDetails(currentWeatherDto));
        weatherReport.setForecastReport(getWeatherForecast(weatherForecastDto));

        return weatherReport;
    }

    private Weather getCurrentWeather(CurrentWeatherDto weatherData) {

        double temp = weatherData.getMain().getTemp();
        double humidity = weatherData.getMain().getHumidity();
        double pressure = weatherData.getMain().getPressure();

        return new Weather(temp, humidity, pressure);
    }

    private WeatherReportDetails getWeatherReportDetails(CurrentWeatherDto weatherData) {
        String city = weatherData.getName();
        String coordinates = String.format("%f,%f", weatherData.getCoord().getLat(), weatherData.getCoord().getLon());
        String unit = "Celsius";

        return new WeatherReportDetails(city, coordinates, unit);
    }

    private List<Forecast> getWeatherForecast(WeatherForecastDto weatherForecastDto) {
        List<Forecast> forecastList = new ArrayList<>();

        DateFormat dtoFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        for (ForecastDto forecastDto : weatherForecastDto.getList()) {
            Date dtoDate;

            try {
                dtoDate = dtoFormat.parse(forecastDto.getDt_txt());

                // Dirty workaround for getting the daily forecast out of Api as the endpoint that does it itself
                // is only for paid subscribers.
                if (!forecastList.isEmpty()) {
                    Date lastDateInForecast = dateFormat.parse(forecastList.get(forecastList.size() - 1).getDate());
                    if (lastDateInForecast.getDate() == dtoDate.getDate()) {
                        continue;
                    }
                }

                if (dtoDate.getDate() == new Date().getDate() || dtoDate.before(new Date())) {
                    continue;
                }

            } catch (ParseException e) {
                continue;
            }

            String forecastDate = dateFormat.format(dtoDate);

            Weather weather = new Weather();
            weather.setHumidity(forecastDto.getMain().getHumidity());
            weather.setPressure(forecastDto.getMain().getPressure());
            weather.setTemperature(forecastDto.getMain().getTemp());

            Forecast forecast = new Forecast(forecastDate, weather);

            forecastList.add(forecast);

            if (forecastList.size() == 3) {
                break;
            }
        }

        return forecastList;
    }
}

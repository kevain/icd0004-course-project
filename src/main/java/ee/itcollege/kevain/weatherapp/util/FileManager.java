package ee.itcollege.kevain.weatherapp.util;

import ee.itcollege.kevain.weatherapp.model.WeatherReport;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileManager {
    public static void writeWeatherReportToFile(WeatherReport weatherReport) throws IOException {

        String fileName = String.format("WeatherReport-%s", weatherReport.getWeatherReportDetails().getCity());
        String contents = convertWeatherReportObjectToJsonString(weatherReport);

        try (PrintWriter printWriter = new PrintWriter(fileName + ".json")){
            printWriter.print(contents);
        }
    }

    public static List<String> readCityNamesFromFile(String pathToFile) throws IOException {
        return Files.readAllLines(Paths.get(pathToFile));
    }

    private static String convertWeatherReportObjectToJsonString(WeatherReport weatherReport) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);

        return objectMapper.writeValueAsString(weatherReport);
    }
}

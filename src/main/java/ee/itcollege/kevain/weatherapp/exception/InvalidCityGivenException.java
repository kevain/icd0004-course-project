package ee.itcollege.kevain.weatherapp.exception;

public class InvalidCityGivenException extends Exception {

    public InvalidCityGivenException(String message) {
        super(message);
    }
}

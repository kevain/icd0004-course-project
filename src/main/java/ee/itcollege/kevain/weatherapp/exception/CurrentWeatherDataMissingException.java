package ee.itcollege.kevain.weatherapp.exception;

public class CurrentWeatherDataMissingException extends Exception{

    public CurrentWeatherDataMissingException() {
        super();
    }

    public CurrentWeatherDataMissingException(String message) {
        super(message);
    }
}

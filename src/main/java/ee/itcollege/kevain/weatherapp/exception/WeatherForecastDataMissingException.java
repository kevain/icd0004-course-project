package ee.itcollege.kevain.weatherapp.exception;

public class WeatherForecastDataMissingException extends Exception {

    public WeatherForecastDataMissingException() {
    }

    public WeatherForecastDataMissingException(String message) {
        super(message);
    }
}

package ee.itcollege.kevain.weatherapp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WeatherReport {
    private WeatherReportDetails weatherReportDetails;
    private Weather currentWeatherReport;
    private List<Forecast> forecastReport;
}

package ee.itcollege.kevain.weatherapp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WeatherReportDetails {
    private String city;
    private String coordinates;
    private String temperatureUnit;
}

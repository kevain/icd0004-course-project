package ee.itcollege.kevain.weatherapp.api.integration;

import com.sun.jersey.api.client.ClientResponse;
import ee.itcollege.kevain.weatherapp.api.OpenWeatherMapApi;
import ee.itcollege.kevain.weatherapp.api.dto.CurrentWeatherDto;
import ee.itcollege.kevain.weatherapp.api.dto.WeatherDto;
import ee.itcollege.kevain.weatherapp.exception.InvalidCityGivenException;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class OpenWeatherMapApiTest {

    private OpenWeatherMapApi api;

    @Before
    public void setApi() {
        api = new OpenWeatherMapApi();
    }

    @Test
    public void shouldReturnOkWhenValidCityCurrentWeatherIsRequested() {
        String city = "Tartu";
        ClientResponse clientResponse = api.getCurrentWeather(city);

        assertThat(clientResponse.getStatus(), is(HttpStatus.SC_OK));
    }

    @Test
    public void shouldReturnOkWhenValidCityWeatherForecastIsRequested() {
        String city = "Helsinki";
        ClientResponse clientResponse = api.getWeatherForecast(city);

        assertThat(clientResponse.getStatus(), is(HttpStatus.SC_OK));
    }

    @Test
    public void shouldReturn404WhenInvalidCityCurrentWeatherIsRequested() {
        String city = "acl wep2rio";
        ClientResponse clientResponse = api.getCurrentWeather(city);

        assertThat(clientResponse.getStatus(), is(HttpStatus.SC_NOT_FOUND));
    }

    @Test
    public void shouldReturn404WhenInvalidCityWeatherForecastIsRequested() {
        String city = "129dpjwqico;a";
        ClientResponse clientResponse = api.getCurrentWeather(city);

        assertThat(clientResponse.getStatus(), is(HttpStatus.SC_NOT_FOUND));
    }

    @Test
    public void shouldReturnWeatherDataForSameCityItWasAskedFor() throws InvalidCityGivenException {
        String city = "Tallinn";
        CurrentWeatherDto currentWeatherDto = api.getCurrentWeatherDataForCity(city);

        boolean responseContainsOriginalCity = currentWeatherDto.getName().toLowerCase().equals(city.toLowerCase());

        assertThat(responseContainsOriginalCity, is(true));
    }

    @Test
    public void shouldHaveTemperatureInCurrentWeatherResponse() throws InvalidCityGivenException {
        String city = "London";
        WeatherDto weatherDto = api.getCurrentWeatherDataForCity(city).getMain();

        assertThat(weatherDto.getTemp(), notNullValue());
    }

    @Test
    public void shouldHaveHumidityInCurrentWeatherResponse() throws InvalidCityGivenException {
        String city = "London";
        WeatherDto weatherDto = api.getCurrentWeatherDataForCity(city).getMain();

        assertThat(weatherDto.getHumidity(), notNullValue());
    }

    @Test
    public void shouldHavePressureInCurrentWeatherResponse() throws InvalidCityGivenException {
        String city = "London";
        WeatherDto weatherDto = api.getCurrentWeatherDataForCity(city).getMain();

        assertThat(weatherDto.getPressure(), notNullValue());
    }

    @Test
    public void shouldHavePressureInWeatherForecastResponse() throws InvalidCityGivenException {
        String city = "London";
        WeatherDto weatherDto = api.getWeatherForecastForCity(city).getList().get(0).getMain();

        assertThat(weatherDto.getPressure(), notNullValue());
    }

    @Test
    public void shouldHaveTemperatureInWeatherForecastResponse() throws InvalidCityGivenException {
        String city = "London";
        WeatherDto weatherDto = api.getWeatherForecastForCity(city).getList().get(0).getMain();

        assertThat(weatherDto.getTemp(), notNullValue());
    }

    @Test
    public void shouldHaveHumidityInWeatherForecastResponse() throws InvalidCityGivenException {
        String city = "London";
        WeatherDto weatherDto = api.getWeatherForecastForCity(city).getList().get(0).getMain();

        assertThat(weatherDto.getHumidity(), notNullValue());
    }

    @Test
    public void shouldHaveStringDateInWeatherForecastResponse() throws InvalidCityGivenException {
        String city = "London";
        String date = api.getWeatherForecastForCity(city).getList().get(0).getDt_txt();

        assertThat(date, notNullValue());
    }

}

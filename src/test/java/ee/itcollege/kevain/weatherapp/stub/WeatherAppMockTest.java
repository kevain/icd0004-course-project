package ee.itcollege.kevain.weatherapp.stub;

import ee.itcollege.kevain.weatherapp.WeatherApp;
import ee.itcollege.kevain.weatherapp.api.OpenWeatherMapApi;
import ee.itcollege.kevain.weatherapp.api.dto.CoordinatesDto;
import ee.itcollege.kevain.weatherapp.api.dto.CurrentWeatherDto;
import ee.itcollege.kevain.weatherapp.api.dto.WeatherDto;
import ee.itcollege.kevain.weatherapp.api.dto.WeatherForecastDto;
import ee.itcollege.kevain.weatherapp.exception.CurrentWeatherDataMissingException;
import ee.itcollege.kevain.weatherapp.exception.InvalidCityGivenException;
import ee.itcollege.kevain.weatherapp.exception.WeatherForecastDataMissingException;
import ee.itcollege.kevain.weatherapp.model.WeatherReport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class WeatherAppMockTest {

    private WeatherApp weatherApp;

    @Mock
    OpenWeatherMapApi openWeatherMapApiMock;

    @Before
    public void setUp() {
        weatherApp = new WeatherApp(openWeatherMapApiMock);
    }

    @Test
    public void shouldReturnAWeatherReportWhenValidCityWasGiven() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        String city = "Paide";

        CurrentWeatherDto currentWeatherDto = new CurrentWeatherDto();
        currentWeatherDto.setName(city);
        currentWeatherDto.setMain(new WeatherDto());
        currentWeatherDto.setCoord(new CoordinatesDto());

        WeatherForecastDto weatherForecastDto = new WeatherForecastDto();
        weatherForecastDto.setList(new ArrayList<>());

        Mockito.when(openWeatherMapApiMock.getCurrentWeatherDataForCity(anyString())).thenReturn(currentWeatherDto);
        Mockito.when(openWeatherMapApiMock.getWeatherForecastForCity(anyString())).thenReturn(weatherForecastDto);

        WeatherReport weatherReport = weatherApp.getWeatherReportForCity(city);

        assertEquals(weatherReport.getWeatherReportDetails().getCity(), city);
    }

}

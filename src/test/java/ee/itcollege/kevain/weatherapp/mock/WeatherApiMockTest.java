package ee.itcollege.kevain.weatherapp.mock;

import ee.itcollege.kevain.weatherapp.WeatherApp;
import ee.itcollege.kevain.weatherapp.api.OpenWeatherMapApi;
import ee.itcollege.kevain.weatherapp.api.dto.CurrentWeatherDto;
import ee.itcollege.kevain.weatherapp.exception.CurrentWeatherDataMissingException;
import ee.itcollege.kevain.weatherapp.exception.InvalidCityGivenException;
import ee.itcollege.kevain.weatherapp.exception.WeatherForecastDataMissingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class WeatherApiMockTest {

    private WeatherApp weatherApp;

    @Mock
    OpenWeatherMapApi openWeatherMapApiMock;

    @Before
    public void setUp() {
        weatherApp = new WeatherApp(openWeatherMapApiMock);
    }

    @Test
    public void shouldNotCallApiWhenNoCityHasBeenGiven() {
        try {
            weatherApp.getWeatherReportForCity(null);
        } catch (Exception e) {
            // ignored
        }

        verifyZeroInteractions(openWeatherMapApiMock);
    }

    @Test
    public void shouldNotCallApiWhenEmptyStringIsPassedAsCity() {
        try {
            weatherApp.getWeatherReportForCity("");
        } catch (Exception e) {
            // ignored
        }

        verifyZeroInteractions(openWeatherMapApiMock);
    }

    @Test
    public void shouldCallApiWhenCityIsProvided() throws InvalidCityGivenException {
        String city = "Tartu";

        when(openWeatherMapApiMock.getCurrentWeatherDataForCity(anyString())).thenReturn(mock(CurrentWeatherDto.class));

        try {
            weatherApp.getWeatherReportForCity(city);
        } catch (CurrentWeatherDataMissingException | WeatherForecastDataMissingException e) {
            // ignored
        }

        verify(openWeatherMapApiMock).getCurrentWeatherDataForCity(city);
    }
}

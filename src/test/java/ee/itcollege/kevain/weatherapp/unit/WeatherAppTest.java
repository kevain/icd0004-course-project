package ee.itcollege.kevain.weatherapp.unit;

import ee.itcollege.kevain.weatherapp.WeatherApp;
import ee.itcollege.kevain.weatherapp.exception.CurrentWeatherDataMissingException;
import ee.itcollege.kevain.weatherapp.exception.InvalidCityGivenException;
import ee.itcollege.kevain.weatherapp.exception.WeatherForecastDataMissingException;
import ee.itcollege.kevain.weatherapp.model.Forecast;
import ee.itcollege.kevain.weatherapp.model.Weather;
import ee.itcollege.kevain.weatherapp.model.WeatherReport;
import ee.itcollege.kevain.weatherapp.model.WeatherReportDetails;
import ee.itcollege.kevain.weatherapp.util.FileManager;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class WeatherAppTest {

    private WeatherApp weatherApp;

    @Before
    public void setUp() {
        weatherApp = new WeatherApp();
    }

    @Test
    public void shouldHaveDetailsOnWeatherReport() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Tallinn");
        WeatherReportDetails weatherReportDetails = weatherReport.getWeatherReportDetails();

        assertNotNull(weatherReportDetails);
    }

    @Test
    public void shouldHaveCurrentWeatherOnWeatherReport() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Tartu");
        Weather currentWeatherReport = weatherReport.getCurrentWeatherReport();

        assertNotNull(currentWeatherReport);
    }

    @Test
    public void shouldHaveForecastReportOnWeatherReport() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Narva");
        List<Forecast> forecastReport = weatherReport.getForecastReport();

        assertNotNull(forecastReport);
    }

    @Test
    public void weatherReportDetailsShouldIncludeTheCity() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Paide");
        WeatherReportDetails weatherReportDetails = weatherReport.getWeatherReportDetails();
        String city = weatherReportDetails.getCity();

        assertNotNull(city);
    }

    @Test
    public void weatherReportShouldBeAboutTheSameCityItWasQueriedFor() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        String expectedCity = "Rapla";

        WeatherReport weatherReport = weatherApp.getWeatherReportForCity(expectedCity);
        WeatherReportDetails weatherReportDetails = weatherReport.getWeatherReportDetails();
        String weatherReportDetailsCity = weatherReportDetails.getCity();

        assertEquals(expectedCity, weatherReportDetailsCity);
    }

    @Test
    public void weatherReportDetailsShouldHaveCoordinatesForQueriedCity() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Tapa");
        WeatherReportDetails weatherReportDetails = weatherReport.getWeatherReportDetails();
        String coordinates = weatherReportDetails.getCoordinates();

        assertNotNull(coordinates);
    }

    @Test
    public void weatherReportDetailsCoordinatesShouldHaveLonAndLatSeparatedByComma() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Haapsalu");
        WeatherReportDetails weatherReportDetails = weatherReport.getWeatherReportDetails();
        String coordinates = weatherReportDetails.getCoordinates();

        assertEquals(2, coordinates.split(",").length);
    }

    @Test
    public void weatherReportDetailsShouldHaveATemperatureUnit() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Kuressaare");
        WeatherReportDetails weatherReportDetails = weatherReport.getWeatherReportDetails();
        String temperatureUnit = weatherReportDetails.getTemperatureUnit();

        assertNotNull(temperatureUnit);
    }

    @Test
    public void weatherReportDetailsTemperatureUnitShouldBeCelsius() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Kuusalu");
        WeatherReportDetails weatherReportDetails = weatherReport.getWeatherReportDetails();
        String temperatureUnit = weatherReportDetails.getTemperatureUnit();

        assertEquals("CELSIUS", temperatureUnit.toUpperCase());
    }

    @Test
    public void currentWeatherReportShouldHaveTemperature() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Tallinn");
        Weather currentWeatherReport = weatherReport.getCurrentWeatherReport();

        double temperature = currentWeatherReport.getTemperature();
    }

    @Test
    public void currentWeatherReportShouldHaveHumidity() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Tallinn");
        Weather currentWeatherReport = weatherReport.getCurrentWeatherReport();

        double humidity = currentWeatherReport.getHumidity();
    }

    @Test
    public void currentWeatherReportShouldHavePressure() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Tallinn");
        Weather currentWeatherReport = weatherReport.getCurrentWeatherReport();

        double pressure = currentWeatherReport.getPressure();
    }

    @Test
    public void shouldHaveForecastForThreeDaysInForecastReport() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Tartu");
        List<Forecast> forecastReport = weatherReport.getForecastReport();

        assertEquals(3, forecastReport.size());
    }

    @Test
    public void firstForecastReportElementShouldBeForTomorrow() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Loksa");
        Forecast forecast = weatherReport.getForecastReport().get(0);
        String forecastDate = forecast.getDate();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date expectedDate = calendar.getTime();
        String expectedDateString = new SimpleDateFormat("yyyy-MM-dd").format(expectedDate);

        assertEquals(expectedDateString, forecastDate);
    }

    @Test
    public void secondForecastReportElementShouldBeForTwoDaysFromNow() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Tapa");
        Forecast forecast = weatherReport.getForecastReport().get(1);
        String forecastDate = forecast.getDate();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        Date expectedDate = calendar.getTime();
        String expectedDateString = new SimpleDateFormat("yyyy-MM-dd").format(expectedDate);

        assertEquals(expectedDateString, forecastDate);
    }

    @Test
    public void thirdForecastReportElementShouldBeForThreeDaysForNow() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Keila");
        Forecast forecast = weatherReport.getForecastReport().get(2);
        String forecastDate = forecast.getDate();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 3);
        Date expectedDate = calendar.getTime();
        String expectedDateString = new SimpleDateFormat("yyyy-MM-dd").format(expectedDate);

        assertEquals(expectedDateString, forecastDate);
    }

    @Test
    public void firstForecastOnForecastReportShouldHaveAWeatherForecast() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Tallinn");
        Forecast forecast = weatherReport.getForecastReport().get(0);
        Weather weather = forecast.getWeather();

        assertNotNull(weather);
    }

    @Test
    public void secondForecastOnForecastReportShouldHaveAWeatherForecast() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Tartu");
        Forecast forecast = weatherReport.getForecastReport().get(1);
        Weather weather = forecast.getWeather();

        assertNotNull(weather);
    }

    @Test
    public void thirdForecastOnForecastReportShouldHaveAWeatherForecast() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Antsla");
        Forecast forecast = weatherReport.getForecastReport().get(2);
        Weather weather = forecast.getWeather();

        assertNotNull(weather);
    }

    @Test(expected = InvalidCityGivenException.class)
    public void shouldThrowAnExceptionWhenNoCityIsGiven() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity(null);
    }

    @Test(expected = InvalidCityGivenException.class)
    public void shouldThrowAnExceptionWhenEmptyCityIsGiven() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("");
    }

    @Test(expected = InvalidCityGivenException.class)
    public void shouldThrowAnExceptionWhenANonExistantCityIsGiven() throws InvalidCityGivenException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        weatherApp.getWeatherReportForCity("asdfghjkl");
    }

    @Test
    public void shouldBeAbleToWriteWeatherReportToFile() throws InvalidCityGivenException, IOException,
            WeatherForecastDataMissingException, CurrentWeatherDataMissingException {
        WeatherReport weatherReport = weatherApp.getWeatherReportForCity("Tallinn");
        FileManager.writeWeatherReportToFile(weatherReport);
    }

    @Test
    public void shouldBeAbleToReadAFile() throws IOException {
        List<String> data = FileManager.readCityNamesFromFile("src\\test\\resources\\demo-input-file.txt");
        assertNotNull(data);
    }

    @Test
    public void shouldBeAbleToReadLineByLineFromInputFile() throws IOException {
        List<String> data = FileManager.readCityNamesFromFile("src\\test\\resources\\demo-input-file.txt");
        assertEquals(4, data.size());
    }

}

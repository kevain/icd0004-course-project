package ee.itcollege.kevain.weatherapp.util.stub;

import ee.itcollege.kevain.weatherapp.WeatherApp;
import ee.itcollege.kevain.weatherapp.exception.CurrentWeatherDataMissingException;
import ee.itcollege.kevain.weatherapp.exception.InvalidCityGivenException;
import ee.itcollege.kevain.weatherapp.exception.WeatherForecastDataMissingException;
import ee.itcollege.kevain.weatherapp.model.WeatherReport;
import ee.itcollege.kevain.weatherapp.util.FileManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(FileManager.class)
public class FileManagerTest {

    @Test
    public void shouldReturnWeatherReportForTheCityReadFromFile() throws IOException, InvalidCityGivenException, CurrentWeatherDataMissingException, WeatherForecastDataMissingException {
        List<String> citiesString = Arrays.asList("Vilnius");

        mockStatic(Paths.class);
        mockStatic(Files.class);

        when(FileManager.readCityNamesFromFile(anyString())).thenReturn(citiesString);

        WeatherReport weatherReport = new WeatherApp().getWeatherReportForCity(FileManager.readCityNamesFromFile("fileName").get(0));

        assertEquals(weatherReport.getWeatherReportDetails().getCity(), citiesString.get(0));
    }
}

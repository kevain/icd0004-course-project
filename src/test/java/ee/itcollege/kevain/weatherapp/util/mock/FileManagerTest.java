package ee.itcollege.kevain.weatherapp.util.mock;

import ee.itcollege.kevain.weatherapp.util.FileManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.PrintWriter;

import static org.powermock.api.mockito.PowerMockito.verifyZeroInteractions;

@RunWith(PowerMockRunner.class)
@PrepareForTest(FileManager.class)
public class FileManagerTest {

    @Mock
    PrintWriter printWriter;

    @Test
    public void shouldNotWriteAFileIfNoWeatherReportWasProvidedToFileManagerForWriting() {

        try {
            FileManager.writeWeatherReportToFile(null);
        } catch (Exception e) {
            // ignored
        }

        verifyZeroInteractions(printWriter);
    }
}

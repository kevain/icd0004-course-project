package ee.itcollege.kevain.web.page_object;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class UiTestBase {

    WebDriver webDriver;

    @BeforeClass
    public void setUpWebDriver() {
        WebDriverManager.chromedriver().setup();
        webDriver = new ChromeDriver(new ChromeOptions().addArguments("--incognito", "headless"));
    }

    @AfterClass
    public void closeWebDriver() {
        if (webDriver != null) {
            webDriver.close();
        }
    }
}

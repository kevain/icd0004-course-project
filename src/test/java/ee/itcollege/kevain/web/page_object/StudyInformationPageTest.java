package ee.itcollege.kevain.web.page_object;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class StudyInformationPageTest extends UiTestBase {

    private StudyInformationPage studyInformationPage;

    @BeforeMethod
    public void getStudyInformationPage() {
        webDriver.get(StudyInformationPage.URL);
        studyInformationPage = new StudyInformationPage(webDriver);
    }

    @Test
    public void shouldMentionHighQualityEducationInStudyInformationPage() {
        String expected = "kvaliteetne haridus";
        String actual = studyInformationPage.getHeaderText().toLowerCase();

        boolean pageContainsExpectedText = actual.contains(expected);

        assertThat(pageContainsExpectedText, is(true));
    }

}
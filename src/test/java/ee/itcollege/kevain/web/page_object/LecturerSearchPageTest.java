package ee.itcollege.kevain.web.page_object;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LecturerSearchPageTest extends UiTestBase {

    private LecturerSearchPage lecturerSearchPage;

    @BeforeMethod
    public void getLecturerSearchPage() {
        webDriver.get(LecturerSearchPage.URL);
        lecturerSearchPage = new LecturerSearchPage(webDriver);
    }

    @Test
    public void shouldHaveGermanMummaAtTaltechDotEeForOurLecturersEmailAddress(){
        String name = "German Mumma";
        lecturerSearchPage.enterLecturerName(name);
        lecturerSearchPage.clickSearchButton();

        String expectedEmail = "german.mumma@taltech.ee";
        String actualEmail = lecturerSearchPage.getLecturerEmailValue().trim().toLowerCase();

        assertThat(actualEmail, is(expectedEmail));
    }

}
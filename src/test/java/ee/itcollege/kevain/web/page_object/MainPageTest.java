package ee.itcollege.kevain.web.page_object;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MainPageTest extends UiTestBase {

    private MainPage mainPage;

    @BeforeMethod
    public void getMainPage() {
        webDriver.get(MainPage.URL);
        mainPage = new MainPage(webDriver);
    }

    @Test
    public void shouldGoToOisWhenOisButtonIsClicked() {
        mainPage.clickGoToOisButton();

        String expectedUrl = "https://ois2.ttu.ee/";
        String actualUrl = webDriver.getCurrentUrl();

        boolean wasDirectedToExpectedPage = actualUrl.startsWith(expectedUrl);

        assertThat(wasDirectedToExpectedPage, is(true));
    }

    @Test
    public void shouldGoToMoodleWhenOisButtonIsClicked() {
        mainPage.clickGoToMoodleButton();

        String expectedUrl = "https://moodle.taltech.ee/";
        String actualUrl = webDriver.getCurrentUrl();

        boolean wasDirectedToExpectedPage = actualUrl.startsWith(expectedUrl);

        assertThat(wasDirectedToExpectedPage, is(true));
    }

    @Test
    public void shouldGoToSiseveebWhenOisButtonIsClicked() {
        mainPage.clickGoToSiseveebButton();

        String expectedUrl = "https://auth.ttu.ee/";
        String actualUrl = webDriver.getCurrentUrl();

        boolean wasDirectedToExpectedPage = actualUrl.startsWith(expectedUrl);

        assertThat(wasDirectedToExpectedPage, is(true));
    }

}